FROM centos:7.9.2009

ENV PATH="/root/.local/bin:${PATH}"
ENV LD_LIBRARY_PATH="/usr/lib64:/usr/local/lib:${LD_LIBRARY_PATH}"
ENV LIBRARY_PATH="/usr/local/lib:${LIBRARY_PATH}"

RUN yum install -y gcc gcc-c++ make \
    glibc-static libstdc++-static zlib-static expat-static \
    gmp-static cairo-devel pango-devel libxml2-devel gmp-devel \
    git sudo ncurses ncurses-devel

RUN mkdir -p ~/.local/bin
RUN curl -Lk https://www.stackage.org/stack/linux-x86_64 | \
    tar xz --strip-components=1 -C ~/.local/bin